<?php
/*
Plugin Name: Import Events
Plugin URI: https://bitbucket.org/ivankostic/import-events/
Description: Import Events using WP-CLI in JSON format
Author: Ivan Kostic
Version: 1.0.0
Author URI: https://awebdeveloper.co.uk
*/
defined( 'ABSPATH' ) || exit; // exit if accessed directly

if ( !class_exists( 'Import_Events' )) :

class Import_Events {

    /**
     * Data fetched from remote server
     *
     * @var object
     */
    protected $_data;
    protected $_mailto = 'logging@agentur-loop.com';


    /**
     * Class constrtuctor
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    public function __construct() {
        $this->define_constants();
        $this->includes();
        $this->init();
    }

    /**
     * Defines constants for use in plugin globally
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function define_constants() {
        $this->define( 'IE_ABSURL', plugins_url( '/', __FILE__ ));
        $this->define( 'IE_ABSPATH', dirname( __FILE__ ) . '/' );
        $this->define( 'IE_BASENAME', plugin_basename( __FILE__ ));
        $this->define( 'EVENTS_CPT', 'events' );
    }


    /**
    * Required WP actions
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    private function init() {
        add_action( 'cli_init', array( $this, 'awd_cli_register_commands' ));
        add_action( 'admin_menu', array( $this, 'export_events_page' ));
    }


    /**
     * Include some files
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function includes() {
        include_once( IE_ABSPATH . '/includes/class-install-ie.php' );
    }


    /**
     * Define constant if not already set.
     *
     * @param  string $name
     * @param  string|bool $value
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function define( $name, $value ) {
        if ( !defined( $name ) ) {
            define( $name, $value );
        }
    }


    /**
     * Registers our command when cli get's initialized.
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    public function awd_cli_register_commands() {
    	WP_CLI::add_command( 'importevents', 'Import_Events' );
    }


    /**
    * Run main command
    * wp importevents from https://api.npoint.io/4f08a37ba4aa277c9ed5
    *
    * Sample Data stored @ https://api.npoint.io/
    * Read: https://api.npoint.io/4f08a37ba4aa277c9ed5
    * Write (Open in browser to Edit): https://www.npoint.io/docs/4f08a37ba4aa277c9ed5
    *
    * @param  string URL to JSON data
    * @since  1.0.0
    * @author Ivan Kostic
    */
    public function from( $args ) {
        $url = $args[0];
        WP_CLI::line( "Importing events from:  $url..." );

        $this->getData( $url );
        $this->handleData();
    }


    /**
     * Method to get remote data
     *
     * @param  string URL to JSON data
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function getData( $url ) {
        $content = @file_get_contents( $url );
        if ( $content === FALSE ) {
            WP_CLI::error( "Failed to open $url!" );
        }

        $this->_data = json_decode( $content );
        WP_CLI::line( "JSON data from $url parsed successfully..." );
    }


    /**
     * Import new or update existing data for each ID
     * Writes logs on command prompt, and triggers email notification
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function handleData() {
        $log      = array( 'inserted_num' => 0, 'updated_num' => 0 );
        $progress = WP_CLI\Utils\make_progress_bar( 'Importing Events', count( $this->_data ));

        foreach ( $this->_data as $event ) {
            $args = array(
                'import_id'   => $event->id,
                'post_type'   => EVENTS_CPT,
                'post_title'  => $event->title ? $event->title : NULL,
                'post_status' => $event->isActive ? 'publish' : 'draft',
                'post_date'   => $event->timestamp,
                'meta_input'  => array(
                    'about'     => $event->about ? $event->about : NULL,
                    'organizer' => $event->organizer ? $event->organizer : NULL,
                    'email'     => $event->email ? $event->email : NULL,
                    'address'   => $event->address ? $event->address : NULL,
                    'longitude' => $event->longitude ? $event->longitude : NULL,
                    'latitude'  => $event->latitude ? $event->latitude : NULL
                )
            );

            // Dealing with custom tax
            wp_set_object_terms( $event->id, $event->tags, EVENTS_CPT . '_tag' );

            if ( !get_post_status( $event->id )) {
                try {
                    wp_insert_post( $args, true );
                    $log['inserted_num']++;
                } catch ( Exception $e ) {
                    WP_CLI::warning( "Failed to insert event with ID: $event->id ($event->title)!" );
                    WP_CLI::warning( "$e->message" );
                }

            } else {
                try {
                    $args['ID'] = $event->id;
                    wp_update_post( $args, true );
                    $log['updated_num']++;
                } catch ( Exception $e ) {
                    WP_CLI::warning( "Failed to update event with ID: $event->id ($event->title)!" );
                    WP_CLI::warning( "$e->message" );
                }

            }

            $progress->tick();
        }
        $progress->finish();

        WP_CLI::line( "!------------------------------------!" );
        WP_CLI::line( "-Number of inserted events: " . $log['inserted_num'] );
        WP_CLI::line( "-Number of updated events: " . $log['updated_num'] );
        WP_CLI::line( "-Total number of events imported: " . ($log['inserted_num'] + $log['updated_num']) );

        $this->email_notification( $log );
        WP_CLI::line( "Notification log sent to $this->_mailto!" );
        WP_CLI::success( 'Events import successfully completed!' );
    }


    /**
     * Sends a notification log to an email
     *
     * @param  string email
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function email_notification( $log ) {
        $to = $this->_mailto;
        $subject = 'Import Events Log - Script by Ivan Kostic';
        $body = "<p>Hello,</p><br>";
        $body .= "<p>The email is an automatic log triggered by <strong>Import Events</strong> script!</p><br>";
        $body .= "<p>- Number of inserted events: <strong>" . $log['inserted_num'] . "</strong></p>";
        $body .= "<p>- Number of updated events: <strong>" . $log['updated_num'] . "</strong></p>";
        $body .= "<p>-----</p>";
        $body .= "<p>- Total number of events imported: <strong>" . ($log['inserted_num'] + $log['updated_num']) . "</strong></p><br>";
        $body .= "<p><strong>Events import successfully completed!</strong></p><br><br>";
        $body .= "<p>Thanks!</p>";
        $headers = array('Content-Type: text/html; charset=UTF-8');

        wp_mail( $to, $subject, $body, $headers );
    }


    /**
     * Create relative times (needed for FE template)
     *
     * @param  string date and time (U)
     * @return array  [message string, period (required for styling)]
     * @since  1.0.0
     * @author Ivan Kostic
     */
    public static function get_event_time( $event_date ) {
        $event_time = [];
        $event_post_time = $event_date;
        $now = current_time('timestamp');

        if ( $event_post_time < $now ) {
            $event_time['relative'] = human_time_diff( $event_post_time, $now ) . " ago.";
            $event_time['period'] = 'past';
        } else {
            $event_time['relative'] = "Comming in " . human_time_diff( $event_post_time, $now ) . ".";
            $event_time['period'] = 'future';
        }

        return $event_time;
    }


    /**
     * Export data from database to JSON format
     * URL: /wp-admin/?page=exportevents (must be logged in)
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function export_data() {
        $event_arr = array();
        $args = array(
            'post_type' => 'events',
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
        );

        $event = new WP_Query( $args );

        while ( $event->have_posts() ) { $event->the_post();
            // Get custom tags
            $event_tags = [];
            $tags = wp_get_post_terms( get_the_ID(), EVENTS_CPT . '_tag' );

            foreach ( $tags as $tag ) {
                $event_tags[] = $tag->name;
            }

            $event_arr[] = [
                'id'        => get_the_ID(),
                'title'     => get_the_title(),
                'about'     => get_field('about'),
                'organizer' => get_field('organizer'),
                'timestamp' => get_the_date( 'Y-m-d H:i:s' ),
                'isActive'  => get_post_status() != 'draft' ? true : false,
                'email'     => get_field('email'),
                'address'   => get_field('address'),
                'latitude'  => (float) get_field('latitude'),
                'longitude' => (float) get_field('longitude'),
                'tags'      => $event_tags,
            ];
        };

        if ( $event_arr ) {
            $json_obj = json_encode($event_arr, JSON_PRETTY_PRINT);

            echo "<pre>";
            print_r( $json_obj );
            echo "</pre>";
        } else {
            echo "<pre>No data to export! Please, import some data first.</pre>";
        }

        wp_reset_postdata();
    }


    /**
     * Creates a blank admin page for displaying exported data
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    public function export_events_page() {
        $hook = add_submenu_page( null, '', '', 'manage_options', 'exportevents', function() {} );

        add_action('load-' . $hook, function() {
            $this->export_data();
            exit;
        });
    }
}
new Import_Events();

endif;
