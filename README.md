# README #

This README contains info about using the Import Events script.

### What is this repository for? ###

* Import, export and displaying events plugin
* 1.0.0

### How do I get set up? ###

* Summary of set up

Fresh WP installation (desirable), ACF Plugin, WP-CLI.

* Configuration

Make sure to configure emails sending on localhost (for local testing only).

1. Download and install FREE version of ACF plugin.
2. Upload "Import Events" plugin to WordPress and activate it.
3. Copy "events-template" from root of this repo to active theme's directory. Create a new post in the backend, and assign "Events listing" template to it.

* Dependencies

WP-CLI

ACF Plugin (Free version)

### How do I run things? ###

* Import events

To trigger import, open cmd, navigate to WP dir and run "wp importevents from REMOTEURLTOJSON".

For testing purposes you can use this data here [https://www.npoint.io/docs/4f08a37ba4aa277c9ed5](https://www.npoint.io/docs/4f08a37ba4aa277c9ed5). Feel free to edit it as much as you want. You can retrieve this data at [https://api.npoint.io/4f08a37ba4aa277c9ed5](https://api.npoint.io/4f08a37ba4aa277c9ed5).

You can use the import command with data above with "wp importevents from https://api.npoint.io/4f08a37ba4aa277c9ed5"!

* Export events

To export data, you need to reach this URL: (siteurl.com)/wp-admin/?page=exportevents (while you're logged in).

* View events

To see events listing, just navigate to the URL of the post that you created on step #3!

### Who do I talk to? ###

* Ivan Kostic (Plugin developer)

ivan@awebdeveloper.co.uk
[https://awebddeveloper.co.uk](https://awebddeveloper.co.uk)
