<?php
/**
 * Install_IE Class.
 *
 * @author   Ivan Kostic
 * @version  1.0.0
 */

defined( 'ABSPATH' ) || exit; // exit if accessed directly


if ( !class_exists( 'Install_IE', false )) :

/**
 * Install_IE Class.
 */
class Install_IE {

    /**
     * Class constrtuctor
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    public function __construct() {
        $this->install();
    }

    /**
    * Run intallation methods
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    private function install() {
        $this->init_actions();
        $this->includes();
    }


    /**
    * Required WP actions
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    private function init_actions () {
        add_action( 'admin_init', array( $this, 'required_plugins' ));
        add_action( 'init', array( $this, 'register_events_cpt' ));
        add_action( 'init', array( $this, 'register_events_tax' ));
    }


    /**
     * Include some files
     *
     * @since  1.0.0
     * @author Ivan Kostic
     */
    private function includes() {
        include_once( IE_ABSPATH . '/includes/events-custom-fields.php' );
    }


    /**
    * Register custom post type.
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    public function register_events_cpt() {
        register_post_type( EVENTS_CPT, [
            'public'             => true,
            'show_in_nav_menus'  => true,
            'label'              => 'Events',
            'menu_icon'          => 'dashicons-megaphone',
        ]);

        remove_post_type_support( EVENTS_CPT, 'editor' );
    }


    /**
    * Register custom tax.
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    public function register_events_tax() {
        register_taxonomy(
            EVENTS_CPT . '_tag',
            EVENTS_CPT,
        );
    }


    /**
    * Required 3rd-Party plugins
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    public function required_plugins() {
        if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
            add_action( 'admin_notices', array( $this, 'required_plugins_notice' ));
            deactivate_plugins( IE_BASENAME );

            if ( isset( $_GET['activate'] ) ) {
                unset( $_GET['activate'] );
            }
        }
    }


    /**
    * Required 3rd-Party plugins notice to run if they
    * are not active
    *
    * @since  1.0.0
    * @author Ivan Kostic
    */
    public function required_plugins_notice() {
        echo "<div class='error'>
                <p>Sorry, but Import Events Plugin requires the
                <strong>Advanced Custom Fields</strong> plugin to be installed and activated!</p>
             </div>";
    }
}
new Install_IE;

endif;
