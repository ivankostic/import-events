<?php
/**
 * Template Name: Events Listing
 * Author: Ivan Kostic
 * Template Post Type: post, page
 *
 * @package WordPress
 */

get_header();
?>
<div class="events-container">
    <h1 class="page-title"><?php the_title(); ?></h1>
    <?php the_content(); ?>

    <ul class="events">
    <?php
    $args = array(
        'post_type' => 'events',
        'post_status' => array( 'publish', 'future' ),
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    );
    $loop = new WP_Query( $args );

    if ( $loop->have_posts() ) :

        while ( $loop->have_posts() ) : $loop->the_post();
            $event_time = Import_Events::get_event_time( get_the_time('U') );
            ?>
            <li class="event event--<?php echo $event_time['period'] ?>">
                <div class="event__meta">
                    <h2 class="event__title"><?php the_title(); ?></h2>
                    <span class="event__date"><?php echo $event_time['relative'] ?></span>
                </div>
                <div class="event__content">
                    <?php the_field('about'); ?>
                </div>
                <div class="event__footer">
                    <p><?php the_field('address'); ?></p>
                    <p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
                </div>
            </li>
    <?php
        endwhile;

    else:
    ?>
        <li class="event">
            <div class="event__meta">
                <h2 class="event__title">No Events to display</h2>
            </div>
            <div class="event__content">
                <p>Please add/import some events!</p>
            </div>
            <div class="event__footer">
                <p><a href="mailto:ivan@awebdeveloper.co.uk">ivan@awebdeveloper.co.uk</a></p>
            </div>
        </li>
    <?php
    endif;

    wp_reset_postdata();
    ?>
    </div>
</div>
<?php
get_footer();
?>

<style>
.page-title {
    margin-bottom: 30px;
    font-weight: 700;
    font-size: 36px;
}
.events-container {
    max-width: 860px;
    width: 100%;
    margin: 0 auto;
    padding: 0 20px;
}
.events {
    list-style-type: none;
    margin: 50px 0 0;
}
.event {
    overflow: hidden;
    width: 100%;
    font-size: 16px;
    margin-bottom: 40px;
    box-shadow: 0 0 8px 4px #dedede;
    background-color: #fff;
}
.event:nth-child(even) {
    background-color: #f8f8f8;
}
.event--past {
    opacity: 0.7;
}
.event a {
    color: #ff7334;
}
.event__meta {
    display: flex;
    justify-content: space-between;
    margin-bottom: 20px;
    background: #648765;
    color: #fff;
    padding: 20px;
}
.event__content {
    padding: 20px;
}
.event__title {
    font-size: 18px;
    font-weight: 600;
    color: #fff;
    margin-bottom: 0;
}
.event__footer {
    display: flex;
    justify-content: space-between;
    margin-top: 20px;
    font-size: 13px;
    padding: 20px;
    border-top: 1px solid #648765;
}
.event__footer p {
    margin: 0;
}

@media (max-width: 500px) {
    .event__meta,
    .event__footer {
        flex-wrap: wrap;
    }
    .event__title {
        margin-bottom: 10px;
    }
    .event__date {
        font-size: 14px;
    }
}
</style>
